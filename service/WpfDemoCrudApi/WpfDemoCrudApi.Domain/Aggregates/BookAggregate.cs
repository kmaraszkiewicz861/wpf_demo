﻿using WpfDemoCrudApi.Domain.Entities;
using WpfDemoCrudApi.Domain.Services.Commnads;

namespace WpfDemoCrudApi.Domain.Aggregates
{
    public class BookAggregate
    {
        public Book Book => _book;

        private Book _book;

        public BookAggregate(AddBookCommand model)
        {
            _book = new Book
            {
                Title = model.Title,
                Author = model.Author,
                CategoryId = model.CategoryId,
                Description = model.Description,
            };
        }

        public BookAggregate(UpdateBookCommand model)
        {
            _book = new Book
            {
                BookId = model.BookId,
                Title = model.Title,
                Author = model.Author,
                CategoryId = model.CategoryId,
                Description = model.Description,
            };
        }

        public BookAggregate(Book book)
        {
            _book = book;
        }
    }
}
