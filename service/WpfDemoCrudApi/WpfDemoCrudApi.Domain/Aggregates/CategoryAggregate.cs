﻿using WpfDemoCrudApi.Domain.Entities;

namespace WpfDemoCrudApi.Domain.Aggregates
{
    public class CategoryAggregate
    {
        public Category Category { get; }

        public CategoryAggregate(Category category)
        {
            Category = category;
        }
    }
}
