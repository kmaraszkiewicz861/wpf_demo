﻿using FluentValidation;
using WpfDemoCrudApi.Domain.Services.Commnads;

namespace WpfDemoCrudApi.Domain.Validators
{
    public class DeleteBookCommandValidator : AbstractValidator<DeleteBookCommand>
    {
        public DeleteBookCommandValidator()
        {
            RuleFor(c => c.BookId)
                .GreaterThan(0)
                .NotEmpty();
        }
    }
}
