﻿using FluentValidation;
using WpfDemoCrudApi.Domain.Services.Commnads;

namespace WpfDemoCrudApi.Domain.Validators
{
    public class UpdateBookCommandValidator : AbstractValidator<UpdateBookCommand>
    {
        public UpdateBookCommandValidator()
        {
            RuleFor(c => c.BookId)
                .GreaterThan(0)
                .NotEmpty();

            RuleFor(c => c.Title)
                .NotEmpty()
                .MaximumLength(50);

            RuleFor(c => c.Description)
                .NotEmpty()
                .MaximumLength(5000);

            RuleFor(c => c.Description)
                .NotEmpty()
                .MaximumLength(150);

            RuleFor(c => c.CategoryId)
                .GreaterThan(0)
                .NotEmpty();
        }
    }
}
