﻿using FluentValidation;
using WpfDemoCrudApi.Domain.Services.Commnads;

namespace WpfDemoCrudApi.Domain.Validators
{
    public class AddBookCommandValidator : AbstractValidator<AddBookCommand>
    {
        public AddBookCommandValidator()
        {
            RuleFor(c => c.Title)
                .NotEmpty()
                .MaximumLength(50);

            RuleFor(c => c.Description)
                .NotEmpty()
                .MaximumLength(5000);

            RuleFor(c => c.Description)
                .NotEmpty()
                .MaximumLength(150);

            RuleFor(c => c.CategoryId)
                .NotEmpty();
        }
    }
}
