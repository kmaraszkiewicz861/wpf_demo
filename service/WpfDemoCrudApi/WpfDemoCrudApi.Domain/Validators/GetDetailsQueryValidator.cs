﻿using FluentValidation;
using WpfDemoCrudApi.Domain.Services.Queries;

namespace WpfDemoCrudApi.Domain.Validators
{
    public class GetDetailsQueryValidator : AbstractValidator<GetDetailsQuery>
    {
        public GetDetailsQueryValidator()
        {
            RuleFor(c => c.BookId)
                .GreaterThan(0)
                .NotEmpty();
        }
    }
}
