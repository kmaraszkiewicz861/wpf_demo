﻿using MediatR;
using WpfDemoCrudApi.Domain.Models;

namespace WpfDemoCrudApi.Domain.Services.Queries
{
    public class GetAllQuery : IRequest<IEnumerable<BookModel>>
    {

    }
}
