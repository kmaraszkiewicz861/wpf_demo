﻿using MediatR;
using WpfDemoCrudApi.Domain.Models;

namespace WpfDemoCrudApi.Domain.Services.Queries
{
    public class GetAllCategoriesQuery : IRequest<IEnumerable<CategoryModel>>
    {

    }
}
