﻿using MediatR;
using WpfDemoCrudApi.Domain.Models;

namespace WpfDemoCrudApi.Domain.Services.Queries
{
    public class GetDetailsQuery : IRequest<BookModel>
    {
        public int BookId { get; set; }
    }
}
