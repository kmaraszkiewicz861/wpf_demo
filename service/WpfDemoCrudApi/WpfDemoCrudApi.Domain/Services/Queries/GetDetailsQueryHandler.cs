﻿using AutoMapper;
using FluentValidation;
using MediatR;
using WpfDemoCrudApi.Domain.Aggregates;
using WpfDemoCrudApi.Domain.Models;
using WpfDemoCrudApi.Domain.Repositories;

namespace WpfDemoCrudApi.Domain.Services.Queries
{
    public class GetDetailsQueryHandler : IRequestHandler<GetDetailsQuery, BookModel>
    {
        private readonly IValidator<GetDetailsQuery> _validator;

        private readonly IBookRepository _bookRepository;

        private readonly IMapper _mapper;

        public GetDetailsQueryHandler(IValidator<GetDetailsQuery> validator, IBookRepository bookRepository, IMapper mapper)
        {
            _validator = validator;
            _bookRepository = bookRepository;
            _mapper = mapper;
        }

        public async Task<BookModel> Handle(GetDetailsQuery request, CancellationToken cancellationToken)
        {
            _validator.ValidateAndThrow(request);

            BookAggregate bookAggregate = await _bookRepository.GetDetailsAsync(request.BookId, cancellationToken);

            return _mapper.Map<BookAggregate, BookModel>(bookAggregate);
        }
    }
}
