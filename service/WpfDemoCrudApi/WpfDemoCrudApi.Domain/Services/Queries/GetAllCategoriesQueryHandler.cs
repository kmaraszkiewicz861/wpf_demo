﻿using AutoMapper;
using MediatR;
using WpfDemoCrudApi.Domain.Aggregates;
using WpfDemoCrudApi.Domain.Models;
using WpfDemoCrudApi.Domain.Repositories;

namespace WpfDemoCrudApi.Domain.Services.Queries
{
    public class GetAllCategoriesQueryHandler : IRequestHandler<GetAllCategoriesQuery, IEnumerable<CategoryModel>>
    {
        private readonly ICategoryService _categoryService;

        private readonly IMapper _mapper;

        public GetAllCategoriesQueryHandler(ICategoryService categoryService, IMapper mapper)
        {
            _categoryService = categoryService;
            _mapper = mapper;
        }

        public Task<IEnumerable<CategoryModel>> Handle(GetAllCategoriesQuery request, CancellationToken cancellationToken)
        {
            IEnumerable<CategoryAggregate> categoryAggregates = _categoryService.GetAll();

            return Task.FromResult(
                _mapper.Map<IEnumerable<CategoryAggregate>, IEnumerable<CategoryModel>>(categoryAggregates));
        }
    }
}
