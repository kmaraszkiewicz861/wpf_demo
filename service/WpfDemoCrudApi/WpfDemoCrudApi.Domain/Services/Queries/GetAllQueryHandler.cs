﻿using AutoMapper;
using MediatR;
using WpfDemoCrudApi.Domain.Aggregates;
using WpfDemoCrudApi.Domain.Models;
using WpfDemoCrudApi.Domain.Repositories;

namespace WpfDemoCrudApi.Domain.Services.Queries
{
    public class GetAllQueryHandler : IRequestHandler<GetAllQuery, IEnumerable<BookModel>>
    {
        private readonly IBookRepository _bookRepository;

        private readonly IMapper _mapper;

        public GetAllQueryHandler(IBookRepository bookRepository, IMapper mapper)
        {
            _bookRepository = bookRepository;
            _mapper = mapper;
        }

        public Task<IEnumerable<BookModel>> Handle(GetAllQuery request, CancellationToken cancellationToken)
        {
            IEnumerable<BookAggregate> books = _bookRepository.GetAll();

            var bookModels = _mapper.Map<IEnumerable<BookAggregate>, IEnumerable<BookModel>>(books);

            return Task.FromResult(bookModels);
        }
    }
}
