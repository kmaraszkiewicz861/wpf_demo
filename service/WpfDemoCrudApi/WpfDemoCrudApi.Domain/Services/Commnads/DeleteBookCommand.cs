﻿using MediatR;

namespace WpfDemoCrudApi.Domain.Services.Commnads
{
    public class DeleteBookCommand : IRequest
    {
        public int BookId { get; set; }
    }
}
