﻿using FluentValidation;
using MediatR;
using WpfDemoCrudApi.Domain.Aggregates;
using WpfDemoCrudApi.Domain.Repositories;

namespace WpfDemoCrudApi.Domain.Services.Commnads
{
    public class UpdateBookCommandHandler : IRequestHandler<UpdateBookCommand>
    {
        private readonly IValidator<UpdateBookCommand> _validator;

        private readonly IBookRepository _bookRepository;

        public UpdateBookCommandHandler(IValidator<UpdateBookCommand> validator, IBookRepository bookRepository)
        {
            _validator = validator;
            _bookRepository = bookRepository;
        }

        public async Task<Unit> Handle(UpdateBookCommand request, CancellationToken cancellationToken)
        {
            _validator.ValidateAndThrow(request);

            var aggregate = new BookAggregate(request);

            await _bookRepository.UpdateAsync(aggregate, cancellationToken);

            return Unit.Value;
        }
    }
}
