﻿using AutoMapper;
using FluentValidation;
using MediatR;
using WpfDemoCrudApi.Domain.Aggregates;
using WpfDemoCrudApi.Domain.Models;
using WpfDemoCrudApi.Domain.Repositories;

namespace WpfDemoCrudApi.Domain.Services.Commnads
{
    public class AddBookCommandHandler : IRequestHandler<AddBookCommand, BookModel>
    {
        private readonly IBookRepository _bookRepository;

        private readonly IValidator<AddBookCommand> _validator;

        private readonly IMapper _mapper;

        public AddBookCommandHandler(IBookRepository bookRepository, IValidator<AddBookCommand> validator, IMapper mapper)
        {
            _bookRepository = bookRepository;
            _validator = validator;
            _mapper = mapper;
        }

        public async Task<BookModel> Handle(AddBookCommand request, CancellationToken cancellationToken)
        {
            _validator.ValidateAndThrow(request);

            var bookAggregate = new BookAggregate(request);

            var createdBookAggregate = await _bookRepository.AddAsync(bookAggregate, cancellationToken);

            return _mapper.Map<BookAggregate, BookModel>(createdBookAggregate);
        }
    }
}
