﻿using FluentValidation;
using MediatR;
using WpfDemoCrudApi.Domain.Repositories;

namespace WpfDemoCrudApi.Domain.Services.Commnads
{
    public class DeleteBookCommandHandler : IRequestHandler<DeleteBookCommand>
    {
        private readonly IValidator<DeleteBookCommand> _validator;

        private readonly IBookRepository _bookRepository;

        public DeleteBookCommandHandler(IValidator<DeleteBookCommand> validator, IBookRepository bookRepository)
        {
            _validator = validator;
            _bookRepository = bookRepository;
        }

        public async Task<Unit> Handle(DeleteBookCommand request, CancellationToken cancellationToken)
        {
            _validator.ValidateAndThrow(request);

            await _bookRepository.RemoveAsync(request.BookId);

            return Unit.Value;
        }
    }
}
