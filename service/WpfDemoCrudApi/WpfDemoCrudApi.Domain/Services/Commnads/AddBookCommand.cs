﻿using MediatR;
using WpfDemoCrudApi.Domain.Models;

namespace WpfDemoCrudApi.Domain.Services.Commnads
{
    public class AddBookCommand : IRequest<BookModel>
    {
        public string Title { get; set; }

        public string Description { get; set; }

        public string Author { get; set; }

        public int CategoryId { get; set; }
    }
}
