﻿using AutoMapper;
using WpfDemoCrudApi.Domain.Aggregates;
using WpfDemoCrudApi.Domain.Models;

namespace WpfDemoCrudApi.Domain.Profiles
{
    public class QueryModelsProfile : Profile
    {
        public QueryModelsProfile() 
        {
            CreateMap<BookAggregate, BookModel>()
                .ForMember(dest => dest.BookId, act => act.MapFrom(src => src.Book.BookId))
                .ForMember(dest => dest.Title, act => act.MapFrom(src => src.Book.Title))
                .ForMember(dest => dest.Description, act => act.MapFrom(src => src.Book.Description))
                .ForMember(dest => dest.Author, act => act.MapFrom(src => src.Book.Author))
                .ForMember(dest => dest.CategoryId, act => act.MapFrom(src => src.Book.CategoryId))
                .ForMember(dest => dest.CategoryName, act => act.MapFrom(src => src.Book.Category.Name));

            CreateMap<CategoryAggregate, CategoryModel>()
                .ForMember(dest => dest.CategoryId, act => act.MapFrom(src => src.Category.CategoryId))
                .ForMember(dest => dest.Name, act => act.MapFrom(src => src.Category.Name));
        }
    }
}
