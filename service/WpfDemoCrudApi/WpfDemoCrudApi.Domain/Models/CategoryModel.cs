﻿namespace WpfDemoCrudApi.Domain.Models
{
    public class CategoryModel
    {
        public int CategoryId { get; set; }

        public string Name { get; set; }
    }
}
