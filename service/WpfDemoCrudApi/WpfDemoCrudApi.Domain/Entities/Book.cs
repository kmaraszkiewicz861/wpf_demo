﻿namespace WpfDemoCrudApi.Domain.Entities
{
    public class Book
    {
        public int BookId { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public string Author { get; set; }

        public int CategoryId { get; set; }

        public Category Category { get; set; }
    }
}
