﻿using WpfDemoCrudApi.Domain.Aggregates;
namespace WpfDemoCrudApi.Domain.Repositories
{
    public interface IBookRepository
    {
        Task<BookAggregate> AddAsync(BookAggregate book, CancellationToken cancellationToken);
        IEnumerable<BookAggregate> GetAll();
        Task<BookAggregate> GetDetailsAsync(int id, CancellationToken cancellationToken);
        Task RemoveAsync(int id);
        Task UpdateAsync(BookAggregate bookAggregate, CancellationToken cancellationToken);
    }
}
