﻿using WpfDemoCrudApi.Domain.Aggregates;

namespace WpfDemoCrudApi.Domain.Repositories
{
    public interface ICategoryService
    {
        IEnumerable<CategoryAggregate> GetAll();
    }
}
