﻿using System.Reflection;
using CWpfDemoCrudApi.Infrastructure;
using CWpfDemoCrudApi.Infrastructure.Repositories;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using WpfDemoCrudApi.Domain.Repositories;
using WpfDemoCrudApi.Domain.Services.Commnads;
using WpfDemoCrudApi.Domain.Validators;

namespace CWpfDemoCrudApi.Configuration
{
    public static class ServiceProviderExtension
    {
        public static IServiceCollection ConfigureRepositories(this IServiceCollection services)
        {
            services.AddScoped<IBookRepository, BookRepository>();
            services.AddScoped<ICategoryService, CategoryService>();

            return services;
        }

        public static IServiceCollection ConfigureMediatr(this IServiceCollection services)
        {
            services.AddMediatR(Assembly.GetAssembly(typeof(AddBookCommandHandler))!);

            return services;
        }

        public static IServiceCollection ConfigureDatabase(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<BookDbContext>(
                options => options.UseSqlServer(configuration["ConnectionString"]));

            return services;
        }

        public static IServiceCollection ConfigureFluentValidation(this IServiceCollection services)
        {
            services.AddValidatorsFromAssemblyContaining<AddBookCommandValidator>();

            return services;
        }
    }
}