﻿using CWpfDemoCrudApi.Infrastructure.Exceptions;
using Microsoft.EntityFrameworkCore;
using WpfDemoCrudApi.Domain.Aggregates;
using WpfDemoCrudApi.Domain.Entities;
using WpfDemoCrudApi.Domain.Repositories;

namespace CWpfDemoCrudApi.Infrastructure.Repositories
{
    public class BookRepository : IBookRepository
    {
        private readonly BookDbContext _bookDbContext;

        public BookRepository(BookDbContext bookDbContext)
        {
            _bookDbContext = bookDbContext;
        }

        public async Task<BookAggregate> AddAsync(BookAggregate book, CancellationToken cancellationToken)
        {
            Category category = await _bookDbContext.Category.FirstOrDefaultAsync(c => c.CategoryId == book.Book.CategoryId, cancellationToken);

            if (category == null)
            {
                throw new BookDbContextException("Category not found");
            }

            var result = await _bookDbContext.Books.AddAsync(new Book
            {
                Title = book.Book.Title,
                Author = book.Book.Author,
                Description = book.Book.Description,
                Category = category!
            });

            _bookDbContext.SaveChanges();

            return new BookAggregate(result.Entity);
        }

        public IEnumerable<BookAggregate> GetAll()
        {
            return _bookDbContext.Books.Select(b => new BookAggregate(b));
        }

        public async Task<BookAggregate> GetDetailsAsync(int id, CancellationToken cancellationToken)
        {
            var book = await _bookDbContext
                                .Books
                                .Include(b => b.Category)
                                .FirstOrDefaultAsync(b => b.BookId == id, cancellationToken);

            if (book == null)
            {
                throw new BookDbContextException("Book not found");
            }

            return new BookAggregate(book);
        }

        public async Task RemoveAsync(int id)
        {
            if (id <= 0)
            {
                throw new BookDbContextException("Book doesnt have valid book id");
            }

            var book = await _bookDbContext.Books.FirstOrDefaultAsync(b => b.BookId== id);

            if (book == null)
            {
                throw new BookDbContextException("Book not found");
            }

            _bookDbContext.Books.Remove(book);
            _bookDbContext.SaveChanges();
        }

        public async Task UpdateAsync(BookAggregate bookAggregate, CancellationToken cancellationToken)
        {
            if (bookAggregate.Book.BookId <= 0)
            {
                throw new BookDbContextException("Book doesnt have valid book id");
            }

            Category category = await _bookDbContext.Category.FirstOrDefaultAsync(c => c.CategoryId == bookAggregate.Book.CategoryId, cancellationToken);

            if (category == null)
            {
                throw new BookDbContextException("Category not found");
            }

            Book book = await _bookDbContext.Books.FirstOrDefaultAsync(b => b.BookId == bookAggregate.Book.BookId, cancellationToken);

            if (book == null)
            {
                throw new BookDbContextException("Book not found");
            }

            book.Title = bookAggregate.Book.Title;
            book.Author = bookAggregate.Book.Author;
            book.Description = bookAggregate.Book.Description;
            book.Category = category;

            _bookDbContext.Books.Update(book);
            _bookDbContext.SaveChanges();
        }
    }
}
