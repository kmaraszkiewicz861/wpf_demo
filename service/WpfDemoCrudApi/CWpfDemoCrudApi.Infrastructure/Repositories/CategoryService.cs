﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfDemoCrudApi.Domain.Aggregates;
using WpfDemoCrudApi.Domain.Repositories;

namespace CWpfDemoCrudApi.Infrastructure.Repositories
{
    public class CategoryService : ICategoryService
    {
        private readonly BookDbContext _bookDbContext;

        public CategoryService(BookDbContext bookDbContext)
        {
            _bookDbContext = bookDbContext;
        }

        public IEnumerable<CategoryAggregate> GetAll() =>
            _bookDbContext.Category.Select(c => new CategoryAggregate(c));
    }
}
