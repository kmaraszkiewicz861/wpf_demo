﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CWpfDemoCrudApi.Infrastructure.Exceptions
{
    public class BookDbContextException : Exception
    {
        public BookDbContextException(string? message) : base(message)
        {
        }
    }
}
