﻿using CWpfDemoCrudApi.Infrastructure.Extensions;
using Microsoft.EntityFrameworkCore;
using WpfDemoCrudApi.Domain.Entities;

namespace CWpfDemoCrudApi.Infrastructure
{
    public class BookDbContext : DbContext
    {
        public DbSet<Book> Books { get; set; }

        public DbSet<Category> Category { get; set; }

        public BookDbContext(DbContextOptions options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ConfigureCategoryEntity();
            modelBuilder.SeedCategoryEntities();

            modelBuilder.ConfigureBookEntity();
            modelBuilder.SeedBookEntities();

            base.OnModelCreating(modelBuilder);
        }
    }
}