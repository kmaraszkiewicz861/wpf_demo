﻿using Microsoft.EntityFrameworkCore;
using WpfDemoCrudApi.Domain.Entities;

namespace CWpfDemoCrudApi.Infrastructure.Extensions
{
    internal static class BookEntityExtension
    {
        public static void ConfigureBookEntity(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Book>()
                .HasKey(x => x.BookId);

            modelBuilder.Entity<Book>()
                .Property(x => x.Title).IsRequired().HasMaxLength(50);

            modelBuilder.Entity<Book>()
                .Property(x => x.Description).IsRequired().HasMaxLength(5000);

            modelBuilder.Entity<Book>()
                .Property(x => x.Author).IsRequired().HasMaxLength(150);
        }

        public static void SeedBookEntities(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Book>().HasData(new Book
            {
                BookId = 1,
                CategoryId = 1,
                Title = "Anna Karenina",
                Author = "Leo Tolstoy",
                Description = "Anna Karenina tells of the doomed love affair between the sensuous and rebellious Anna and the dashing officer, Count Vronsky. Tragedy unfolds as Anna rejects her passionless marriage and must endure the hypocrisies of society. Set against a vast and richly textured canvas of nineteenth-century Russia, the novel's seven major characters create a dynamic imbalance, playing out the contrasts of city and country life and all the variations on love and family happiness. While previous versions have softened the robust, and sometimes shocking, quality of Tolstoy's writing, Pevear and Volokhonsky have produced a translation true to his powerful voice. This award-winning team's authoritative edition also includes an illuminating introduction and explanatory notes. Beautiful, vigorous, and eminently readable, this Anna Karenina will be the definitive text for generations to come."
            }, new Book
            {
                BookId = 2,
                CategoryId = 1,
                Title = "Madame Bovary",
                Author = "Gustave Flaubert",
                Description = "For daring to peer into the heart of an adulteress and enumerate its contents with profound dispassion, the author of Madame Bovary was tried for \"offenses against morality and religion.\" What shocks us today about Flaubert's devastatingly realized tale of a young woman destroyed by the reckless pursuit of her romantic dreams is its pure artistry: the poise of its narrative structure, the opulence of its prose (marvelously captured in the English translation of Francis Steegmuller), and its creation of a world whose minor figures are as vital as its doomed heroine. In reading Madame Bovary, one experiences a work that remains genuinely revolutionary almost a century and a half after its creation."
            }, new Book
            {
                BookId = 3,
                CategoryId = 1,
                Title = "War and Peace",
                Author = "Leo Tolstoy",
                Description = "Epic in scale, War and Peace delineates in graphic detail events leading up to Napoleon's invasion of Russia, and the impact of the Napoleonic era on Tsarist society, as seen through the eyes of five Russian aristocratic families.",
            }, new Book
            {
                BookId = 4,
                CategoryId = 1,
                Title = "The Great Gatsby",
                Author = "F. Scott Fitzgerald",
                Description = "The novel chronicles an era that Fitzgerald himself dubbed the \"Jazz Age\". Following the shock and chaos of World War I, American society enjoyed unprecedented levels of prosperity during the \"roaring\" 1920s as the economy soared. At the same time, Prohibition, the ban on the sale and manufacture of alcohol as mandated by the Eighteenth Amendment, made millionaires out of bootleggers and led to an increase in organized crime, for example the Jewish mafia. Although Fitzgerald, like Nick Carraway in his novel, idolized the riches and glamor of the age, he was uncomfortable with the unrestrained materialism and the lack of morality that went with it, a kind of decadence.",
            }, new Book
            {
                BookId = 5,
                CategoryId = 1,
                Title = "Lolita",
                Author = "Vladimir Nabokov",
                Description = "The book is internationally famous for its innovative style and infamous for its controversial subject: the protagonist and unreliable narrator, middle aged Humbert Humbert, becomes obsessed and sexually involved with a twelve-year-old girl named Dolores Haze.",
            });
        }
    }
}
