﻿using Microsoft.EntityFrameworkCore;
using WpfDemoCrudApi.Domain.Entities;

namespace CWpfDemoCrudApi.Infrastructure.Extensions
{
    internal static class CategoryEntityExtension
    {
        public static void ConfigureCategoryEntity(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Category>()
                .HasKey(x => x.CategoryId);

            modelBuilder.Entity<Category>().Property(x => x.Name).IsRequired().HasMaxLength(150);
        }

        public static void SeedCategoryEntities(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Category>().HasData(new Category
            {
                CategoryId = 1,
                Name = "Triller"
            }, new Category
            {
                CategoryId = 2,
                Name = "Horror"
            }, new Category
            {
                CategoryId = 3,
                Name = "Comedy"
            }, new Category
            {
                CategoryId = 4,
                Name = "Sci-Fi"
            });
        }
    }
}
