﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace CWpfDemoCrudApi.Infrastructure
{
    public class BookContextFactory : IDesignTimeDbContextFactory<BookDbContext>
    {
        public BookDbContext CreateDbContext(string[] args)
        {
            var optionsBuilder = new DbContextOptionsBuilder<BookDbContext>();
            optionsBuilder.UseSqlServer("Server=USER;Database=bookStore;Trusted_Connection=True;TrustServerCertificate=True;");

            return new BookDbContext(optionsBuilder.Options);
        }
    }
}