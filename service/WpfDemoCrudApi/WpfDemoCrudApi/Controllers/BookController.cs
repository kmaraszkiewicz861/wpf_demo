using AutoMapper;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using WpfDemoCrudApi.Domain.Models;
using WpfDemoCrudApi.Domain.Services.Commnads;
using WpfDemoCrudApi.Domain.Services.Queries;
using WpfDemoCrudApi.Models.Request;

namespace WpfDemoCrudApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class BookController : ControllerBase
    {
        private readonly IMediator _mediator;

        private readonly IMapper _mapper;

        public BookController(IMediator mediator, IMapper mapper)
        {
            _mediator = mediator;
            _mapper = mapper;
        }

        [HttpGet]
        public ActionResult<IEnumerable<BookModel>> GetAll()
        {
            return Ok(_mediator.Send(new GetAllQuery { }).Result);
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<BookModel>> GetDetailsAsync(int id)
        {
            return Ok(await _mediator.Send(new GetDetailsQuery { BookId = id }));
        }

        [HttpPost]
        public async Task<ActionResult> Add(AddBookRequest model, CancellationToken cancellationToken)
        {
            var command = _mapper.Map<AddBookRequest, AddBookCommand>(model);

            BookModel createdBook = await _mediator.Send(command, cancellationToken);

            return Created($"/Book/{createdBook.BookId}", createdBook);
        }

        [HttpPut]
        public async Task<ActionResult> Update(UpdateBookRequest model, CancellationToken cancellationToken)
        {
            var command = _mapper.Map<UpdateBookRequest, UpdateBookCommand>(model);

            await _mediator.Send(command, cancellationToken);

            return Ok();
        }

        [HttpDelete]
        public async Task<ActionResult> Delete(DeleteBookRequest model, CancellationToken cancellationToken)
        {
            var command = _mapper.Map<DeleteBookRequest, DeleteBookCommand>(model);

            await _mediator.Send(command, cancellationToken);

            return Ok();
        }
    }
}