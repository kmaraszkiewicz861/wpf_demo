﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using WpfDemoCrudApi.Domain.Models;
using WpfDemoCrudApi.Domain.Services.Queries;

namespace WpfDemoCrudApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class CategoryController : ControllerBase
    {
        private readonly IMediator _mediator;

        public CategoryController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<CategoryModel>>> GetAllAsync()
        {
            return Ok(await _mediator.Send(new GetAllCategoriesQuery()));
        }
    }
}