﻿using FluentValidation;
using WpfDemoCrudApi.Models.Responses;

namespace WpfDemoCrudApi.Middleware
{
    public class ExceptionMiddleware
    {
        private readonly RequestDelegate _next;

        public ExceptionMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task InvokeAsync(HttpContext context)
        {
            try
            {
                await _next(context);
            }
            catch (ValidationException ex)
            {
                context.Response.StatusCode = StatusCodes.Status400BadRequest;
                await context.Response.WriteAsJsonAsync(new UnhandledErrorResponse(ex.Errors.Select(e => e.ErrorMessage).ToArray()));
            }
            catch (Exception ex)
            {
                context.Response.StatusCode = StatusCodes.Status500InternalServerError;
                await context.Response.WriteAsJsonAsync(new UnhandledErrorResponse(new[] { ex.Message }));
            }
        }
    }
}
