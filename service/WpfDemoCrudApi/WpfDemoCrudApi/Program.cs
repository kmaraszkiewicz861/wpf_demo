using System.Reflection;
using CWpfDemoCrudApi.Configuration;
using WpfDemoCrudApi.Domain.Profiles;
using WpfDemoCrudApi.Middleware;
using WpfDemoCrudApi.Profiles;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Services
    .ConfigureDatabase(builder.Configuration)
    .ConfigureRepositories()
    .ConfigureFluentValidation()
    .ConfigureMediatr();

builder.Services.AddAutoMapper(Assembly.GetAssembly(typeof(RequestModelsProfile)));
builder.Services.AddAutoMapper(Assembly.GetAssembly(typeof(QueryModelsProfile)));

var app = builder.Build();

app.UseMiddleware<ExceptionMiddleware>();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
