﻿using AutoMapper;
using WpfDemoCrudApi.Domain.Services.Commnads;
using WpfDemoCrudApi.Models.Request;

namespace WpfDemoCrudApi.Profiles
{
    public class RequestModelsProfile : Profile
    {
        public RequestModelsProfile() 
        {
            CreateMap<UpdateBookRequest, UpdateBookCommand>();
            CreateMap<AddBookRequest, AddBookCommand>();
            CreateMap<DeleteBookRequest, DeleteBookCommand>();
        }
    }
}
