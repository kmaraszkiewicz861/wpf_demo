﻿namespace WpfDemoCrudApi.Models.Responses
{
    public record UnhandledErrorResponse(string[] errorMessages);
}
