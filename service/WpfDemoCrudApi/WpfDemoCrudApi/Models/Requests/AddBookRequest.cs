﻿namespace WpfDemoCrudApi.Models.Request
{
    public class AddBookRequest
    {
        public string Title { get; set; }

        public string Description { get; set; }

        public string Author { get; set; }

        public int CategoryId { get; set; }
    }
}
