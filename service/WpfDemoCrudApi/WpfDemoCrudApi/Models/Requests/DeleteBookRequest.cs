﻿namespace WpfDemoCrudApi.Models.Request
{
    public class DeleteBookRequest
    {
        public int BookId { get; set; }
    }
}
