﻿namespace WpfDemoUI.Bindings
{
    public interface ILoadedAction
    {
        void WindowLoaded();
    }
}
