﻿using System;

namespace WpfDemoUI.Bindings
{
    public class LoadedAction : ILoadedAction
    {
        public Action LoadedActionDelegate { get; set; }

        public LoadedAction()
        {
        }

        public LoadedAction(Action action)
        {
            LoadedActionDelegate = action;
        }

        public void WindowLoaded()
        {
            LoadedActionDelegate?.Invoke();
        }
    }
}
