﻿namespace WpfDemoUI.Models
{
    public class BookModel
    {
        public int BookId { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public string Author { get; set; }

        public int CategoryId { get; set; }

        public string CategoryName { get; set; }
    }
}
