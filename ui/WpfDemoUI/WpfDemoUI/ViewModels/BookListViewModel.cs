﻿using System.Collections.ObjectModel;
using System.Threading.Tasks;
using WpfDemoUI.Bindings;
using WpfDemoUI.Models;
using WpfDemoUI.Services;

namespace WpfDemoUI.ViewModels
{
    public class BookListViewModel : BaseViewModel
    {
        public ObservableCollection<BookModel> BookCollection => _bookCollection;
        public ObservableCollection<BookModel> _bookCollection = new ObservableCollection<BookModel>();

        public LoadedAction LoadCommand { get; }

        private readonly BookService _bookService;

        public BookListViewModel()
        {
            LoadCommand = new LoadedAction(async () => await OnLoadCommand());

            _bookService = new BookService(new System.Net.Http.HttpClient());
        }

        private async Task OnLoadCommand()
        {
            var books = await _bookService.GetAll();

            App.Current.Dispatcher.Invoke(() =>
            {
                BookCollection.Clear();
                
                foreach (var book in books)
                {
                    BookCollection.Add(book);
                }
            });
        }
    }
}
