﻿using System;
using WpfDemoUI.Bindings;
using WpfDemoUI.Services;

namespace WpfDemoUI.ViewModels
{
    public class BookFormViewModel : BaseViewModel
    {
        public int _bookId = 0;

        public string Title
        {
            get => _title;
            set
            {
                if (_title == value) return;

                _title = value;
                OnPropertyChanged();
            }
        }

        public string Description
        {
            get => _description;
            set
            {
                if (_description == value) return;

                _description = value;
                OnPropertyChanged();
            }
        }

        public string Author
        {
            get => _author;
            set
            {
                if (_author == value) return;

                _author = value;
                OnPropertyChanged();
            }
        }

        public int CategoryId
        {
            get => _categoryId;
            set
            {
                if (_categoryId == value) return;

                _categoryId = value;
                OnPropertyChanged();
            }
        }

        public string CategoryName
        {
            get => _categoryName;
            set
            {
                if (_categoryName == value) return;

                _categoryName = value;
                OnPropertyChanged();
            }
        }

        public LoadedAction LoadedCommand { get; } = new LoadedAction(OnLoadedCommand);

        private readonly BookService _bookService;

        public BookFormViewModel(BookService bookService)
        {
            _bookService = bookService;
        }

        private string _title;
        private string _description;
        private string _author;
        public int _categoryId;
        private string _categoryName;

        private static void OnLoadedCommand()
        {
        }
    }
}
