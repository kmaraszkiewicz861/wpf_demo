﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Cache;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using WpfDemoUI.Models;

namespace WpfDemoUI.Services
{
    public class BookService
    {
        private readonly string _host = "https://localhost:7087/Book";

        private HttpClient _httpClient;

        public BookService(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public async Task<IEnumerable<BookModel>> GetAll()
        {
            HttpResponseMessage response = await _httpClient.GetAsync(_host);

            response.EnsureSuccessStatusCode();

            var responseStream = await response.Content.ReadAsStreamAsync();

            var bookModels = await JsonSerializer
                                .DeserializeAsync<IEnumerable<BookModel>>(responseStream, 
                                    new JsonSerializerOptions { PropertyNameCaseInsensitive = true });

            return bookModels!;
        }


    }
}
